package io.fumybulb;

import io.fumybulb.app.App;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL46;

public final class Launcher {
    private static volatile int width;
    private static volatile int height;
    private static float t;

    /**
     * Entry point.
     *
     * @param args command-line arguments.
     */
    public static void main(final String[] args) {
        App.MAIN_WINDOW.create(Launcher::resize, Launcher::render, Launcher::handleKey, 800, 600);
    }

    private static void resize(final int w, final int h) {
        assert "main".equals(Thread.currentThread().getName());
        width = w;
        height = h;
    }

    private static void render(final float dt) {
        assert App.Renderer.THREAD_NAME.equals(Thread.currentThread().getName());

        t += dt;
        if (t > 1) {
            App.MAIN_WINDOW.close();
            throw App.Break.INSTANCE;
        }

        GL46.glClear(GL46.GL_COLOR_BUFFER_BIT);
        GL46.glViewport(0, 0, width, height);

        final float aspect = (float) width / height;
        GL46.glMatrixMode(GL46.GL_PROJECTION);
        GL46.glLoadIdentity();
        GL46.glOrtho(-1.0f * aspect, +1.0f * aspect, -1.0f, +1.0f, -1.0f, +1.0f);

        GL46.glMatrixMode(GL46.GL_MODELVIEW);
        GL46.glRotatef(dt * -10.0f, 0, 0, 1);
        GL46.glBegin(GL46.GL_QUADS);
        GL46.glVertex2f(-0.5f, -0.5f);
        GL46.glVertex2f(+0.5f, -0.5f);
        GL46.glVertex2f(+0.5f, +0.5f);
        GL46.glVertex2f(-0.5f, +0.5f);
        GL46.glEnd();
    }

    private static void handleKey(
        final long window,
        final int key,
        @SuppressWarnings("unused") final int scancode,
        final int action,
        @SuppressWarnings("unused") final int mods
    ) {
        if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_RELEASE) {
            GLFW.glfwSetWindowShouldClose(window, true);
        }
    }
}
