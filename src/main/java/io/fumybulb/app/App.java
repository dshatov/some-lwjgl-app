package io.fumybulb.app;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.Callback;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import java.nio.IntBuffer;

public final class App {
    public static final App MAIN_WINDOW = new App();

    private final Object lock = new Object();
    private long window = MemoryUtil.NULL;
    private boolean destroyed;

    private App() {
    }

    public synchronized void create(
        final Resizer resizer,
        final Renderer renderer,
        final GLFWKeyCallbackI keyHandler,
        final int width,
        final int height
    ) {
        destroyed = false;
        final String title = Thread.currentThread().getName();

        if (!GLFW.glfwInit()) {
            throw new RuntimeException("Unable to initialize GLFW.");
        }
        try (
            final GLFWErrorCallback ignored = GLFWErrorCallback.createPrint(System.err).set();
            final GLFWKeyCallback keyCallback = GLFWKeyCallback.create(keyHandler);
            final GLFWFramebufferSizeCallback fsCallback = GLFWFramebufferSizeCallback.create(
                (win, w, h) -> resizer.resize(w, h)
            )
        ) {
            GLFW.glfwDefaultWindowHints();
            GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
            GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE);
            window = GLFW.glfwCreateWindow(width, height, title, MemoryUtil.NULL, MemoryUtil.NULL);
            if (window == MemoryUtil.NULL) {
                throw new RuntimeException("Failed to create the GLFW window.");
            }

            GLFW.glfwSetKeyCallback(window, keyCallback);
            GLFW.glfwSetFramebufferSizeCallback(window, fsCallback);

            final GLFWVidMode vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
            if (vidmode == null) {
                throw new RuntimeException("Failed to get video mode.");
            }
            GLFW.glfwSetWindowPos(window, (vidmode.width() - width) / 2, (vidmode.height() - height) / 2);
            try (final MemoryStack frame = MemoryStack.stackPush()) {
                final IntBuffer framebufferSize = frame.mallocInt(2);
                GLFW.nglfwGetFramebufferSize(window, MemoryUtil.memAddress(framebufferSize),
                    MemoryUtil.memAddress(framebufferSize) + 4
                );
                resizer.resize(framebufferSize.get(0), framebufferSize.get(1));
            }
            GLFW.glfwShowWindow(window);
            final Thread thread = new Thread(() -> renderLoop(renderer), Renderer.THREAD_NAME);
            thread.start();
            winProcLoop();

            synchronized (lock) {
                destroyed = true;
                GLFW.glfwDestroyWindow(window);
            }
            thread.interrupt();
            if (thread.isAlive()) {
                try {
                    thread.join(1000);
                } catch (final InterruptedException e) {
                    thread.setDaemon(true);
                }
            }
        } finally {
            GLFW.glfwTerminate();
            window = MemoryUtil.NULL;
        }
    }

    public void close() {
        if (window != MemoryUtil.NULL) {
            GLFW.glfwSetWindowShouldClose(window, true);
            GLFW.glfwPostEmptyEvent();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    private void renderLoop(final Renderer renderer) {
        GLFW.glfwMakeContextCurrent(window);
        GL.createCapabilities();
        try (final Callback ignored = GLUtil.setupDebugMessageCallback()) {
            GL11.glClearColor(0.3f, 0.5f, 0.7f, 0.0f);
            long lastTime = System.nanoTime();
            while (!destroyed) {
                final long thisTime = System.nanoTime();
                //noinspection CatchMayIgnoreException
                try {
                    renderer.render((thisTime - lastTime) / 1E9f);
                } catch (final Break e) {
                } finally {
                    lastTime = thisTime;
                    synchronized (lock) {
                        if (!destroyed) {
                            GLFW.glfwSwapBuffers(window);
                        }
                    }
                }
            }
        }
    }

    private void winProcLoop() {
        while (!GLFW.glfwWindowShouldClose(window)) {
            GLFW.glfwWaitEvents();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    @FunctionalInterface
    public interface Resizer {
        void resize(final int w, final int h);
    }

    @FunctionalInterface
    public interface Renderer {
        String THREAD_NAME = "renderer";

        void render(final float dt) throws Break;
    }

    public static final class Break extends RuntimeException {
        public static final Break INSTANCE = new Break();

        private Break() {
            super(null, null, false, false);
        }
    }
}
